const creatorsPathPng = "./public/images/creators/png/"
const $showcase = document.querySelector('.showcase-container');
// asian creators
let asianCreators = [{
    file:'Apollo-Gaming-full.png',
    country:'South Asia',
    name:'Apollo Gaming',
},{
    file:'Arpon-Plays-YT-full.png',
    country:'South Asia',
    name:'Arpon Plays',
},{
    file:'HedgeHunt3r-Gaming-full.png',
    country:'South Asia',
    name:'Hedge Hunt3r',
},{
    file:'Kabbo-Gang-full.png',
    country:'South Asia',
    name:'Kabbo Gang',
},{
    file:'Xannat_s-Gaming-full.png',
    country:'South Asia',
    name: 'Xannat'
}]
// indonesian creators
let indoCreators = [{
    file: 'afa-full.png',
    country: 'Indonesia',
    name: 'Afa',
}, {
    file: 'arkaw-full.png',
    country: 'Indonesia',
    name: 'Arkaw',
}, {
    file: 'bkent-full.png',
    country: 'Indonesia',
    name: 'Brandon Kent',
}, {
    file: 'cc-full.png',
    country: 'Indonesia',
    name: 'Citra Cantika',
}, {
    file: 'efdewe-full.png',
    country: 'Indonesia',
    name: 'Efdewe',
}, {
    file: 'entruv-full.png',
    country: 'Indonesia',
    name: 'Entruv',
}, {
    file: 'Hi-Patrick-full.png',
    country: 'Indonesia',
    name: 'Hi Patrick',
}, {
    file: 'Kelvin-Gaming-full.png',
    country: 'Indonesia',
    name: 'Kelvin Gaming',
}, {
    file: 'Letdahyper-full.png',
    country: 'Indonesia',
    name: 'Letdahyper',
}, {
    file: 'Luanluan-full.png',
    country: 'Indonesia',
    name: 'Luanluan',
}, {
    file: 'Luthfi-Halimawan-full.png',
    country: 'Indonesia',
    name: 'Luthfi Halimawan',
}, {
    file: 'Luxxy-full.png',
    country: 'Indonesia',
    name: 'Luxxy',
}, {
    file: 'Muhabeno-full.png',
    country: 'Indonesia',
    name: 'Muhabeno',
}, {
    file: 'Muhammad-Edison-full.png',
    country: 'Indonesia',
    name: 'Edison',
}, {
    file: 'Obbyphy-full.png',
    country: 'Indonesia',
    name: 'Obbyphy',
}, {
    file: 'Olivia-Gosandra-full.png',
    country: 'Indonesia',
    name: 'Olivia',
}, {
    file: 'Oura-Gaming-full.png',
    country: 'Indonesia',
    name: 'Oura Gaming',
}, {
    file: 'Potgame-full.png',
    country: 'Indonesia',
    name: 'Potgame',
}, {
    file: 'Rendy-Rangers-full.png',
    country: 'Indonesia',
    name: 'Rendy Rangers',
}, {
    file: 'Sarah-Viloid-full.png',
    country: 'Indonesia',
    name: 'Sarah Viloid',
}, {
    file: 'Tampan-Gaming-full.png',
    country: 'Indonesia',
    name: 'Tampan Gaming',
}, {
    file: 'Teguh-Suwandi-full.png',
    country: 'Indonesia',
    name: 'Teguh Suwandi',
}, {
    file: 'WawanMKS-full.png',
    country: 'Indonesia',
    name: 'Wawan MKS',
}, {
    file: 'Yudi-Syahputra-full.png',
    country: 'Indonesia',
    name: 'Yudi Syahputra',
}, {
    file: 'zan-full.png',
    country: 'Indonesia',
    name: 'Zan',
}, {
    file: 'zuxxy-full.png',
    country: 'Indonesia',
    name: 'Zuxxy',
}]
// all creators
let allCreators = [...indoCreators, ...asianCreators]

const getArrayCreators = (par) => {
    let result;
    switch (par) {
        case 'all':
            result = allCreators.slice(0, 9);
            break;
        case 'indo':
            result = indoCreators.slice(0, 9);
            break;
        case 'asian':
            break;
        default:
            result = allCreators.slice(0, 9);
            break;
    }
    return result;
}

// selected country
let selected = 'all';
let shows = getArrayCreators(selected)

const showCreators = () => {
    const creators = []
    for (let i = 0; i < shows.length; i++) {
        const html = `<div class="showcase-thumbnail">
            <div class="top">
                <img src="${creatorsPathPng}${shows[i].file}" alt="" srcset="" height="255" width="390">
                <div class="showcase-hover">
                    <div class="country">${shows[i].country}</div>
                    <div class="creator-name">${shows[i].name}</div>
                </div>
            </div>
            <div class="bottom">
            </div>
        </div>`;
        creators.push(html);
    }
    creators.forEach(element => {
        $showcase.insertAdjacentHTML('beforeend', element);
    });
}

const generateRandom = (min, max, exclude) => {
    var num = Math.floor(Math.random() * (max - min + 1)) + min;
    return (num === exclude) ? generateRandom(min, max, exclude) : num;
}

const randomizeShow = () => {
    const rnd = generateRandom(0, 8, 0)
    // const rnd = 0;
    let newRnd;
    switch (selected) {
        case 'all':
            newRnd = randomizeSingle(allCreators)
            shows[rnd] = allCreators[newRnd]
            updateImgByIndex(rnd, allCreators[newRnd])
            break;
        case 'indo':
            newRnd = randomizeSingle(indoCreators)
            shows[rnd] = indoCreators[newRnd]
            updateImgByIndex(rnd, indoCreators[newRnd])
            break;
        case 'asian': 
            newRnd = randomizeSingle(asianCreators)
            shows[rnd] = asianCreators[newRnd]
            updateImgByIndex(rnd, asianCreators[newRnd])
            break;
        default:
            newRnd = randomizeSingle(indoCreators)
            shows[rnd] = indoCreators[newRnd]
            updateImgByIndex(rnd, indoCreators[newRnd])
            break;
    }
}

const randomizeSingle = (creatorsArray) => {
    let found = false
    const newRnd = generateRandom(0, creatorsArray.length - 1, 99)
    shows.forEach(element => {
        if (element === creatorsArray[newRnd])
            found = true
    });
    return (found) ? randomizeSingle(creatorsArray) : newRnd;
}

const updateImgByIndex = (i, item) => {
    $('.showcase-thumbnail').eq(i).find('.bottom').show()

    const prevTop = $('.showcase-thumbnail').eq(i).find('.top').html()
    $('.showcase-thumbnail').eq(i).find('.bottom').html(prevTop)

    const contentTop = `<img src="${creatorsPathPng}${shows[i].file}" alt="" srcset="" height="255" width="390">
    <div class="showcase-hover">
        <div class="country">${shows[i].country}</div>
        <div class="creator-name">${shows[i].name}</div>
    </div>`
    $('.showcase-thumbnail').eq(i).find('.top').html(contentTop)
    $('.showcase-thumbnail').eq(i).find('.top').hide()

    $('.showcase-thumbnail').eq(i).find('.bottom').fadeOut(1000)
    $('.showcase-thumbnail').eq(i).find('.top').fadeIn(1000)

}
